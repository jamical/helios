package com.elvarg.game.entity.impl.player.mode;

import java.util.HashMap;
import java.util.Map;

/**
 * Game Mode Type
 * 08/28/2017
 *
 * @author trees
 */
public enum GameMode {

    NORMAL(3, true, new int[]{13666, 590, 1265, 1351, 841, 1379, 1277}), HARD(2, true, new int[]{}), VETERAN(1, true, new int[]{}), PKER(5, true, new int[]{}), IRONMAN(1, false, new int[]{13666, 590, 1265, 1351, 841, 1379, 1277, 12810, 12811, 12812});

    GameMode(int rate_multiplyer, boolean can_bank, int[] starter) {
        this.multiplier = rate_multiplyer;
        this.bank = can_bank;
        this.starting_items = starter;
    }

    int multiplier;

    boolean bank;

    public int[] getStarting_items() {
        return starting_items;
    }

    int[] starting_items;

    public static Map<Integer, GameMode> getMode_types() {
        return mode_types;
    }

    static Map<Integer, GameMode> mode_types = new HashMap<Integer, GameMode>();

    static {
        for (GameMode type : GameMode.values()) {
            mode_types.put(type.ordinal(), type);
        }
    }

    public GameMode getGameMode(int ordinal) {
        return GameMode.mode_types.get(ordinal);
    }

    public boolean canBank() {
        return bank;
    }
}
