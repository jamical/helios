package com.elvarg.game;

import com.elvarg.game.model.Position;

public class GameConstants {

    public static boolean isDeveloper(String player) {
        switch (player) {
            case "snowman":
            case "keeks":
                return true;
        }
        return false;
    }

    public static final boolean IS_DEADMAN_ENABLED = false;
    public static int WORLD_ID = (IS_DEADMAN_ENABLED ? 2 : 1);
    public static boolean IS_DOUBLE_XP = false;
    public static boolean MySQL_ENABLED = true;
    public static final int GAME_VERSION = 3;
    public static final int GAME_UID = 2;
    public static final String DEFINITIONS_DIRECTORY = "./data/definitions/";
    public static final String CLIPPING_DIRECTORY = "./data/clipping/";
    public static final String DATA_DIR = "./Data2/";
    public static final boolean CONCURRENCY = Runtime.getRuntime().availableProcessors() > 1;
    public static final int GAME_ENGINE_PROCESSING_CYCLE_RATE = 600;
    public static final int QUEUED_LOOP_THRESHOLD = 45;
    public static final Position DEFAULT_POSITION = new Position(3084, 3484);
    public static final String DEFAULT_CLAN_CHAT = "run escape";
    public static final boolean QUEUE_SWITCHING_REFRESH = false;
    public static final int DROP_THRESHOLD = 2;
    public static final double EXP_MULTIPLIER = 12.3D;
    public static final boolean JAGGRAB_ENABLED = false;
    public static final int[] TAB_INTERFACES = {2423, 3917, 44000, 3213, 1644, 5608, -1, 37128, 5065, 5715, 2449, 42500, 147, 32000};

    public static boolean isIsDoubleXp() {
        return IS_DOUBLE_XP;
    }

    public static void setIsDoubleXp(boolean isDoubleXp) {
        IS_DOUBLE_XP = isDoubleXp;
    }

}
