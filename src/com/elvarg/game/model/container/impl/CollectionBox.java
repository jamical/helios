package com.elvarg.game.model.container.impl;

import java.util.Optional;

import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Item;
import com.elvarg.game.model.container.ItemContainer;
import com.elvarg.game.model.container.StackType;

/**
 * Represents a player's inventory item container.
 *
 * @author relex lawl
 */

public class CollectionBox extends ItemContainer {

    /**
     * The Inventory constructor.
     *
     * @param player The player who's inventory is being represented.
     */
    public CollectionBox(Player player) {
        super(player);
    }

    @Override
    public int capacity() {
        return 28;
    }

    @Override
    public StackType stackType() {
        return StackType.DEFAULT;
    }

    @Override
    public CollectionBox refreshItems() {
        getPlayer().getPacketSender().sendItemContainer(this, INTERFACE_ID);
        return this;
    }

    @Override
    public CollectionBox full() {
        getPlayer().getPacketSender().sendMessage("Not enough space in your inventory.");
        return this;
    }

    public static final int INTERFACE_ID = -1;
}
