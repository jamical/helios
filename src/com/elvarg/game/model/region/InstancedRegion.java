package com.elvarg.game.model.region;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import com.elvarg.game.World;
import com.elvarg.game.content.skill.construction.Palette;
import com.elvarg.game.entity.Entity;
import com.elvarg.game.entity.impl.Character;
import com.elvarg.game.entity.impl.npc.NPC;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Action;
import com.elvarg.game.model.Area;
/**
 * Represents an instanced region with entities.
 * @author Professor Oak
 */
public class InstancedRegion {

	public enum RegionInstanceType {
		BARROWS,
		THE_SIX,
		GRAVEYARD,
		FIGHT_CAVE,
		WARRIORS_GUILD,
		NOMAD,
		RECIPE_FOR_DISASTER,
		CONSTRUCTION_HOUSE,
		CONSTRUCTION_DUNGEON,
		KRAKEN;
	}

	private Player owner;
	private RegionInstanceType type;
	private CopyOnWriteArrayList<NPC> npcsList;
	private CopyOnWriteArrayList<Player> playersList;
	private Optional<List<Area>> bounds = Optional.empty();
	
	public InstancedRegion(Player p, RegionInstanceType type) {
		this.owner = p;
		this.type = type;
		this.npcsList = new CopyOnWriteArrayList<NPC>();
		if(type == RegionInstanceType.CONSTRUCTION_HOUSE || type == RegionInstanceType.THE_SIX) {
			this.playersList = new CopyOnWriteArrayList<Player>();
		}
	}

	
	public void sequence(Character character) {
		if(bounds.isPresent()) {
			for(Area area : bounds.get()) {
				//entities.stream().filter(e -> !area.inBounds(e.getPosition())).forEach(e -> removeEntity(e));
				if(!area.inBounds(character.getPosition())) {
					remove(character);
				}
			}
		}
	}
	public void destruct() {
		for(NPC n : npcsList) {
			if(n != null && n.getHitpoints() > 0 && World.getNpcs().get(n.getIndex()) != null && !n.isDying()) {
				 {
				}
			}
		}
		npcsList.clear();
		owner.setRegionInstance(null);
	}

	public void add(Character c) {
		if(type == RegionInstanceType.CONSTRUCTION_HOUSE) {
			if(c.isPlayer()) {
				playersList.add((Player)c);
			} else if(c.isNpc()) {
				npcsList.add((NPC)c);
			}

			if(c.getRegionInstance() == null || c.getRegionInstance() != this) {
				c.setRegionInstance(this);
			}
		}
	}

	public void remove(Character c) {
		if(type == RegionInstanceType.CONSTRUCTION_HOUSE) {
			if(c.isPlayer()) {
				playersList.remove((Player)c);
				if(owner == ((Player)c)) {
					destruct();
				}
			} else if(c.isNpc()) {
				npcsList.remove((NPC)c);
			}

			if(c.getRegionInstance() != null && c.getRegionInstance() == this) {
				c.setRegionInstance(null);
			}
		}
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public RegionInstanceType getType() {
		return type;
	}

	public CopyOnWriteArrayList<NPC> getNpcsList() {
		return npcsList;
	}

	public CopyOnWriteArrayList<Player> getPlayersList() {
		return playersList;
	}

	@Override
	public boolean equals(Object other) {
		return (RegionInstanceType)other == type;
	}
}
