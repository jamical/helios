package com.elvarg.game.content.combat.method.impl.npcs;

import com.elvarg.game.content.combat.CombatType;
import com.elvarg.game.content.combat.hit.PendingHit;
import com.elvarg.game.content.combat.method.CombatMethod;
import com.elvarg.game.entity.impl.Character;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Animation;
import com.elvarg.game.model.Graphic;
import com.elvarg.game.model.GraphicHeight;
import com.elvarg.game.model.Projectile;
import com.elvarg.game.model.SecondsTimer;
import com.elvarg.util.Misc;

public class LavaDragonCombatMethod implements CombatMethod {
	

	private static final Animation MELEE_ATTACK_ANIMATION3 = new Animation(80); // First Attack
	private static final Graphic END_PROJECTILE_GRAPHIC = new Graphic(393, GraphicHeight.HIGH);
	
	
	private SecondsTimer comboTimer = new SecondsTimer();
	private CombatType currentAttackType = CombatType.MELEE;


	public boolean canAttack(Character character, Character target) {
		return true;
	}


	public void preQueueAdd(Character character, Character target) {
		if(currentAttackType == CombatType.MAGIC) {
			new Projectile(character, target, 393, 5, 60, 43, 31, 16).sendProjectile();
		}
	}


	public int getAttackSpeed(Character character) {
		return character.getBaseAttackSpeed();
	}


	public int getAttackDistance(Character character) {
		return 1;
	}


	public void startAnimation(Character character) {
		character.performAnimation(MELEE_ATTACK_ANIMATION3);
	}


	public CombatType getCombatType() {
		return currentAttackType;
	}

	@Override
	public PendingHit[] getHits(Character character, Character target) {
		return new PendingHit[]{new PendingHit(character, target, this, true, 0)};
	}

	@Override
	public void finished(Character character) {
		currentAttackType = CombatType.MELEE;
		if(comboTimer.finished()) {
			if(Misc.getRandom(5) <= 2) {
				//comboTimer.start(5);
				currentAttackType = CombatType.MAGIC;
				character.getCombat().setDisregardDelay(true);
				character.getCombat().doCombat();
			}
		}
	}

	@Override
	public void handleAfterHitEffects(PendingHit hit, Character victim) {
		if(hit.getTarget() == null || !hit.getTarget().isPlayer()) {
			return;
		}
        final Player player = hit.getTarget().getAsPlayer();
         if(currentAttackType == CombatType.MAGIC) {
			player.performGraphic(END_PROJECTILE_GRAPHIC);
		}
		
	}

	
}
