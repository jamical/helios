package com.elvarg.game.content.combat.method.impl.npcs;

import com.elvarg.game.content.combat.CombatType;
import com.elvarg.game.content.combat.hit.PendingHit;
import com.elvarg.game.content.combat.method.CombatMethod;
import com.elvarg.game.entity.impl.Character;
import com.elvarg.game.entity.impl.npc.NPC;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Animation;
import com.elvarg.game.model.Graphic;
import com.elvarg.game.model.GraphicHeight;
import com.elvarg.game.model.Projectile;
import com.elvarg.game.model.SecondsTimer;
import com.elvarg.util.Misc;


public class CerberusCombatMethod implements CombatMethod {
	
	private static final Animation GROUND_ATTACK= new Animation(4492); // Ground Attack
	private static final Animation GHOSTS = new Animation(4494); // Ghosts
	private static final Animation MELEE_ATTACK_ANIMATION3 = new Animation(4493); // First Attack
	private static final Graphic END_PROJECTILE_GRAPHIC = new Graphic(1242, GraphicHeight.HIGH);
	
	
	private NPC npc = new NPC(5867, null); // 1239 1256
	private NPC npc2 = new NPC(5869, null); // 1240 1256
	private NPC npc3 = new NPC(5868, null); // 1241 1256
	
	private SecondsTimer comboTimer = new SecondsTimer();
	private CombatType currentAttackType = CombatType.MELEE;


	public boolean canAttack(Character character, Character target) {
		return true;
	}


	public void preQueueAdd(Character character, Character target) {
		if(currentAttackType == CombatType.MAGIC) {
			new Projectile(character, target, 1242, 1, 60, 43, 31, 16).sendProjectile();
		}
	}


	public int getAttackSpeed(Character character) {
		return character.getBaseAttackSpeed();
	}


	public int getAttackDistance(Character character) {
		return 2;
	}


	public void startAnimation(Character character) {
		character.performAnimation(MELEE_ATTACK_ANIMATION3);
	}


	public CombatType getCombatType() {
		return currentAttackType;
	}

	@Override
	public PendingHit[] getHits(Character character, Character target) {
		return new PendingHit[]{new PendingHit(character, target, this, true, 1)};
	}

	@Override
	public void finished(Character character) {
		currentAttackType = CombatType.MELEE;
		if(comboTimer.finished()) {
			if(Misc.getRandom(5) <= 2) {
				comboTimer.start(5);
				currentAttackType = CombatType.MAGIC;
				character.getCombat().setDisregardDelay(true);
				character.getCombat().doCombat();
			}
		}
	}

	@Override
	public void handleAfterHitEffects(PendingHit hit, Character victim) {
		if(hit.getTarget() == null || !hit.getTarget().isPlayer()) {
			return;
		}
        final Player player = hit.getTarget().getAsPlayer();
         if(currentAttackType == CombatType.MAGIC) {
			player.performGraphic(END_PROJECTILE_GRAPHIC);
		}
		
	}
	
}
