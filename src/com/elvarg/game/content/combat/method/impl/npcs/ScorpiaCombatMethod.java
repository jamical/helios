package com.elvarg.game.content.combat.method.impl.npcs;

import com.elvarg.game.World;
import com.elvarg.game.content.combat.CombatType;
import com.elvarg.game.content.combat.hit.PendingHit;
import com.elvarg.game.content.combat.magic.CombatSpells;
import com.elvarg.game.content.combat.method.CombatMethod;
import com.elvarg.game.entity.impl.Character;
import com.elvarg.game.entity.impl.npc.NPC;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Animation;
import com.elvarg.game.model.Position;
import com.elvarg.game.task.Task;
import com.elvarg.game.task.TaskManager;
import com.elvarg.util.Misc;

public class ScorpiaCombatMethod implements CombatMethod{
	
	private static int babiesKilled = 2;

	public static boolean attackable() {
		return babiesKilled == 2;
	}

	public static void killedBaby() {
		babiesKilled++;
	}
	
	private static final Animation MELEE_ATTACK_ANIMATION = new Animation(4925);

	private CombatType currentAttackType = CombatType.MELEE;

	@Override
	public CombatType getCombatType() {
		return currentAttackType;
	}

	@Override
	public boolean canAttack(Character character, Character target) {
		return true;
	}

	@Override
	public PendingHit[] getHits(Character character, Character target) {
		return new PendingHit[]{new PendingHit(character, target, this, true, 2)};
	}

	@Override
	public void preQueueAdd(Character character, Character target) {
         return;
		}

	@Override
	public int getAttackSpeed(Character character) {
		return character.getBaseAttackSpeed();
	}

	@Override
	public int getAttackDistance(Character character) {
		return 4;
	}

	@Override
	public void startAnimation(Character character) {
		character.performAnimation(MELEE_ATTACK_ANIMATION);
	}

	@Override
	public void finished(Character character) {

		currentAttackType = CombatType.MELEE; {
			
		}
	}
	@Override
	public void handleAfterHitEffects(PendingHit hit, Character entity) {
		if(hit.getTarget() == null || !hit.getTarget().isPlayer()) {
			return;
		}

		final Player player = hit.getTarget().getAsPlayer();
		NPC npc = (NPC)entity;
		npc.performAnimation(new Animation(npc.getDefinition().getAttackAnim()));

		if(npc.getHitpoints() <= 500 && !npc.hasHealed()) {
			NPC[] babies = new NPC[]{new NPC(109, new Position(2854,9642)), new NPC(109, new Position(2854,9631))};
			for(NPC n : babies) {
				World.getAddNPCQueue();
				n.getCombat().attack(player);
				npc.heal(990);
			}
			babiesKilled = 0;
			npc.setHealed(true);
		} else if(npc.hasHealed() && babiesKilled > 0) {
			if(Misc.getRandom(3) == 1) {
				npc.forceChat("You will regret hurting them..");
			}
			TaskManager.submit(new Task(1, npc, false) {
				int tick = 0;
				public void execute() {
					if(tick == 0) {
						npc.prepareSpell(CombatSpells.BABY_SCORPION.getSpell(), player);
						stop();
					}
					tick++;
				}
			});
		}
	}
}
