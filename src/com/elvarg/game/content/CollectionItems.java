package com.elvarg.game.content;

import java.util.HashMap;
import java.util.Map;

/**
 * Collections box data
 *
 * @author trees
 */
public enum CollectionItems {

    ITEM_SET_0(new int[]{565, 558, 556, 559, 1293, 995, 556, 199, 227, 219, 229}), ITEM_SET_1(new int[]{995, 557, 842, 1299, 227, 199, 882, 1027}), ITEM_SET_2(new int[]{217, 213, 557, 215, 995, 207, 882, 1027}), ITEM_SET_3(new int[]{217, 213, 557, 215, 561, 7936, 995, 207, 882, 1027}), ITEM_SET_4(new int[]{217, 1289, 561, 213, 557, 995, 995, 995, 995, 995, 563, 215, 995, 7936, 995, 207, 1213, 882, 1027});

    CollectionItems(int[] i) {
        this.items = i;
    }

    public int[] getItems() {
        return items;
    }

    int[] items;

    static Map<Integer, CollectionItems> types = new HashMap<Integer, CollectionItems>();

    static {
        for (CollectionItems type : CollectionItems.values()) {
            types.put(type.ordinal(), type);
        }
    }
}
