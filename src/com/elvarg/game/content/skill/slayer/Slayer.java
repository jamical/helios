package com.elvarg.game.content.skill.slayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Skill;
import com.elvarg.game.model.dialogue.Dialogue;
import com.elvarg.game.model.dialogue.DialogueExpression;
import com.elvarg.game.model.dialogue.DialogueManager;
import com.elvarg.game.model.dialogue.DialogueType;
import com.elvarg.util.Misc;

/**
 * Its the slayer skill :D
 * Everything to do with slayer is in here.
 * @author Jamix
 *
 */
public class Slayer {
	
	/**
	 * The player
	 */
	Player player;
	
	/**
	 * Constructor
	 * @param player sets the player to this.
	 */
	public Slayer(Player player) {
		this.player = player;
	}
	
	/**
	 * Players task
	 */
	private SlayerTask slayerTask = SlayerTask.NO_TASK;
	
	/**
	 * The amount of npcs in your task.
	 */
	private int taskAmount;
	
	
	/**
	 * Reward the player.
	 * @return
	 */
	public boolean reward() {
		if (player.getSlayer().getSlayerTask() == SlayerTask.NO_TASK) {
			return false;
		}
		player.getSkillManager().addExperience(Skill.SLAYER, player.getSlayer().getSlayerTask().getXp());
		return true;
	}
	
	/**
	 * Can we give them another task?
	 * @return True or False
	 */
	public boolean canDo(Master master) {
		if (player.getSkillManager().getCurrentLevel(Skill.SLAYER) < master.getReq()) {
			String formatedName = master.name();
			String g = String.valueOf(formatedName.charAt(0)).toUpperCase();
			String g1 = formatedName.substring(1);
			formatedName = g + g1;
			DialogueManager.sendStatement(player, "You are not worthy of getting a slayer assignment from " + formatedName + ". Get " + master.getReq() + " before you ask for another assignment.");
		}
		if (player.getSlayer().getSlayerTask() != SlayerTask.NO_TASK) {
			DialogueManager.sendStatement(player, "You already have a slayer task. Kill " + taskAmount +" "+slayerTask.getName() + (taskAmount > 1 ? "s" : "")+".");
			return false;
		}
		return true;
	}
	
	/**
	 * Get us a random task.
	 * @param master
	 */
	public void randomTask(Master master) {
		SlayerTask task = null;
		List<SlayerTask> tasks = new ArrayList<>();
		for (SlayerTask t : SlayerTask.values()) {
			tasks.add(t);
		}
		Collections.shuffle(tasks);
		int i = 0;
		while (task == null) {
			i = Misc.inclusive(0, SlayerTask.values().length-1);
			task = tasks.get(i);
			if (task.levelReq > player.getSkillManager().getCurrentLevel(Skill.SLAYER)) {
				task = null;
				continue;
			}
			if (task == SlayerTask.NO_TASK) {
				task = null;
				continue;
			}
			boolean b = false;
			for (Master m : task.masters) {
				if (m == master) {
					b = true;
				}
			}
			if (!b) {
				task = null;
				continue;
			}
			Random r = new Random();
			this.taskAmount = r.nextInt(200);
			if (taskAmount > 200) {
				taskAmount = 200;
			}
			if (taskAmount < 3) {
				taskAmount = 3;
			}
			
		}
		this.slayerTask = task;
	}
	
	public void decrementAmount() {
		player.getSkillManager().addExperience(Skill.SLAYER, player.getSlayer().getSlayerTask().getXp());
		player.getSlayer().setTaskAmount(player.getSlayer().getTaskAmount()-1);
		player.getPacketSender().sendMessage(player.getSlayer().getTaskAmount() == 0 ? "<col=ff0000>You need to get a new slayer task." : "You got " + player.getSlayer().getTaskAmount() + " left to kill.");
		if (player.getSlayer().getTaskAmount() == 0)
			slayerTask = SlayerTask.NO_TASK;
	}
	
	/**
	 * Get a task.
	 * @param master
	 * @return
	 */
	public boolean getTask(Master master) {
		if (master == null) {
			return false;
		}
		if (!canDo(master)) {
			return true;
		}
		randomTask(master);
		DialogueManager.start(player, new Dialogue() {

			@Override
			public DialogueType type() {
				return DialogueType.NPC_STATEMENT;
			}

			@Override
			public DialogueExpression animation() {
				return DialogueExpression.DEFAULT;
			}
			
			@Override
			public int npcId() {
				return master.id;
			}

			@Override
			public String[] dialogue() {
				return new String[] {"Your new task is to kill " + taskAmount + " " + slayerTask.getName() + (taskAmount > 1 ? "s" : "") + "."};
			}
			
		});
		return true;
	}
	
	public SlayerTask getSlayerTask() {
		return slayerTask;
	}

	public void setSlayerTask(SlayerTask slayerTask) {
		this.slayerTask = slayerTask;
	}

	public int getTaskAmount() {
		return taskAmount;
	}

	public void setTaskAmount(int taskAmount) {
		this.taskAmount = taskAmount;
	}

	/**
	 * An enum that contains the data for our slayer tasks.
	 * @author Jamix
	 *
	 */
	public static enum SlayerTask {
		NO_TASK(0,0,null,"null",0,-1),
//		MOL(1,15,Master.TURAEL,"MOLANISKS",0,1); example
		ABERRANT_SPECTRE(60,90,new Master[] {Master.VANNAKA,Master.CHAELDAR,Master.NIEVE,Master.DURADEL},"Aberrant Spectre",2,3,4,5,6,7),
		ABYSSAL_DEMONS(85,150,new Master[] {Master.NIEVE,Master.DURADEL},"Abyssal Demon",415,416,7241,7410),
		ANKOUS(1,60,new Master[] {Master.VANNAKA,Master.NIEVE,Master.DURADEL},"Ankou",2514,2515,2516,2517,2518,2519,6608,7257),
		BANSHEES(15,22,Master.values(),"Banshee",414,7272,7390),
		BASILISKS(40,75,new Master[] {Master.VANNAKA,Master.CHAELDAR,Master.NIEVE},"Basilisk",417,418,7395),
		BATS(1,8,new Master[] {Master.TURAEL,Master.MAZCHNA},"Bat",2827,2834,3201,4504,6824),
		BEAR(1,30,BATS.getMasters(),"Bear",2839,3908,3909,2838,3423,3424,3425),
		BIRD(1,5,new Master[] {Master.TURAEL},"Bird",2064,2065,2066,2067,2068,4927,5971,5972,5973,1870,2692,2693,2694,2819,2820,2821,2993,3316,3661,3662,3663,6306,6367,6739),
		BLACK_DEMONS(1,157,new Master[] {Master.CHAELDAR,Master.NIEVE,Master.DURADEL},"Black demon",240,1432,2048,2049,2050,2051,2052,5874,5875,5876,5877,6295,6357,7242,7243,7144,7145,7146,7147,7148,7149),
																												
		;
		private int levelReq,xp;//weight;
		private int[] npcs;
		private Master[] masters;
		private String name;
		private SlayerTask(int levelReq, int xp,Master[] masters,String name,int...npcs) {
			this.setLevelReq(levelReq);
			this.setXp(xp);
			this.setNpcs(npcs);
			this.setMasters(masters);
			this.setName(name);
		}
		
		/**
		 * Slayer masters weight. this is for calculating the task a player will get
		 * @param master
		 * @return
		 */
//		public static int mastersWeight(Master master) {
//			int weight = 0;
//			for (SlayerTask t : values()) {
//				if (t == null) {
//					continue;
//				}
//				if (t == NO_TASK) {
//					continue;
//				}
//				for (Master m : t.masters) {
//					if (m == null) {
//						continue;
//					}
//					if (m != master) {
//						continue;
//					}
//					weight += t.weight;
//				}
//				
//			}
//			return weight;
//		}
		
		
		public int getLevelReq() {
			return levelReq;
		}
		public void setLevelReq(int levelReq) {
			this.levelReq = levelReq;
		}
		public int getXp() {
			return xp;
		}
		public void setXp(int xp) {
			this.xp = xp;
		}
		public int[] getNpcs() {
			return npcs;
		}
		public void setNpcs(int[] npcs) {
			this.npcs = npcs;
		}
		public Master[] getMasters() {
			return masters;
		}
		public void setMasters(Master[] masters) {
			this.masters = masters;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
//		public int getWeight() {
//			return weight;
//		}
//		public void setWeight(int weight) {
//			this.weight = weight;
//		}
	}
	
	/**
	 * Slayer master.
	 */
	public static enum Master {
		TURAEL(401,3),
		MAZCHNA(402,20),
		VANNAKA(403,40),
		CHAELDAR(404,70),
		DURADEL(405,100),
		NIEVE(6797,85);
		
		private int id;
		private int req;
		private Master(int id,int req) {
			this.setId(id);
			this.setReq(req);
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getReq() {
			return req;
		}
		public void setReq(int req) {
			this.req = req;
		}
	}
	
	/**
	 * Types of tasks.
	 * @author Jamix
	 *
	 */
	public static enum TaskType {
		EASY,
		MED,
		HARD,
		BOSS;
	}
	
}
