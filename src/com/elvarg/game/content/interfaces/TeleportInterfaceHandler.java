package com.elvarg.game.content.interfaces;

import java.util.HashMap;
import java.util.Map;

import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.teleportation.TeleportHandler;
import com.elvarg.game.model.teleportation.TeleportType;
import com.elvarg.game.model.Position;


public class TeleportInterfaceHandler  {

    /**
     * Handles the button and returns true if it exits
     *
     * @param player
     * @param button_id
     * @return
     */
    static int posX;
    static int posY;
    static int posZ;

    public static boolean actionHandler(Player player, int button_id) {
        Loc location = Loc.types.get(button_id);
        if (location != null) {
            posX = location.x;
            posY = location.y;
            posZ = location.z;
            player.getPacketSender().sendString(41033, location.description[0]);
            player.getPacketSender().sendString(41034, location.description[1]);
            player.getPacketSender().sendString(41035, location.description[2]);
            return true;
        }
        switch(button_id) {
            case 41037:
                if(posX != 0 || posY != 0){
                    TeleportHandler.teleport(player, new Position(posX, posY, posZ), TeleportType.PURO_PURO);
                } else {
                    player.getPacketSender().sendMessage("Please select a destination.");
                }
                break;
            case 41005:
            case 41105:
            case 41205:
            case 41305:
            case 41405:
            case 41705:
                deleteAndReplace(player, 41000);
                break;
            case 41007:
            case 41107:
            case 41207:
            case 41307:
            case 41407:
            case 41707:
                deleteAndReplace(player, 41100);
                break;
            case 41009:
            case 41109:
            case 41209:
            case 41309:
            case 41409:
            case 41709:
                deleteAndReplace(player, 41200);
                break;
            case 41011:
            case 41111:
            case 41211:
            case 41311:
            case 41411:
            case 41711:
                deleteAndReplace(player, 41300);
                break;
            case 41013:
            case 41113:
            case 41213:
            case 41313:
            case 41413:
            case 41713:
                deleteAndReplace(player, 41400);
                break;
            case 41015:
            case 41115:
            case 41215:
            case 41315:
            case 41415:
            case 41715:
                deleteAndReplace(player, 41700);
                break;
        }
        return false;
    }

    static void deleteAndReplace(Player player, int interfaceId) {
        player.getPacketSender().sendInterface(interfaceId);
        posX = 0;
        posY = 0;
        posZ = 0;
        for(int i = 41033; i < 41036; i++) {
            player.getPacketSender().sendString(i, "---");
        }
    }

    /**
     * Stored and accessed data
     */
    public enum Loc {

        ROCK_CRABS(41018, 2672, 3710, 0, new String[]{"Rock crabs have low attack and,", "strength, making them great for", "new players and pures."}),
        YAKS(41020, 2325, 3799, 0, new String[]{"Yaks are a med level monster", "good for med levels as they", "hit less and have more HP."}),
        BANDITS(41022, 3163, 2984, 0, new String[]{"Bandits are aggressive enemies", "that attack on sight, good xp", "when praying protect from melee."}),
        FIRE_GIANTS(41024, 2565, 9884, 0, new String[]{"Fire giants are a high level enemy", "that are known for their rune", "scimitar drops."}),
        EXPERIMENTS(41026, 3555, 9945, 0, new String[]{"Experiments are a high level", "enemy with low attack and", "strength. Good for any level."}),
        HOME(41118, 3106, 3490, 0, new String[]{"The main hub of Gielinor, you", "will find most of the shops", "here in NAME_HERE.."}),
        LUMBRIDGE(41120, 3223, 3218, 0, new String[]{"Lumbridge, the home of the", "duke, you will find goblins to ", "kill and men to pickpocket."}),
        VARROCK(41122, 3210, 3423, 0, new String[]{"Varrock is where you'll", "find the grand exchange and", "various other shops."}),
        FALADOR(41124, 2966, 3380, 0, new String[]{"Falador, home to the", "giant mole, white knights and", "the drop party room."}),
        CAMELOT(41126, 2756, 3478, 0, new String[]{"Camelot, the area connecting", "Catherby and Seers' village.", "You'll find flax and yew trees."}),
        CATHERBY(41128, 2809, 3436, 0, new String[]{"Catherby, a great skilling city,", "here you can fish at high levels", "and plant fruit trees and herbs."}),
        ARDOUGNE(41130, 2661, 3308, 0, new String[]{"Ardougne is a great city for", "thieving, you'll find all the", "spots you need for thieving xp."}),
        YANILLE(41132, 2605, 3094, 0, new String[]{"Yanille is where you'll find", "Nightmare Zone and a home portal", "that's close to a bank."}),
        RELLEKKA(41134, 2659, 3661, 0, new String[]{"Rellekka is home to the Rellekka", "agility course, rock crabs and", "the Rellekka slayer dungeon."}),
        CANIFIS(41136, 3497, 3490, 0, new String[]{"Canifis is where you'll find", "the slayer tower, Canifis agility", "course and Port Phasmatys."}),
        AL_KHARID(41138, 3293, 3176, 0, new String[]{"Al-kharid is home to the", "Duel arena and the gateway to", "the Kharidian desert."}),
        DUEL_ARENA(41218, 3368, 3265, 0, new String[]{"Come here to pit against", "other players in a 1 on 1", "and stake items against their own."}),
        FIGHT_CAVES(41220, 2440, 5172, 0, new String[]{"One of the tougher bosses in", "Run-Escape. Survive 63 waves to", "earn yourself a fire cape."}),
        BARROWS(41222, 3565, 3309, 0, new String[]{"Kill the 6 brothers and break", "into their crypt to find", "valuable high-level items."}),
        INFERNO(41224, 2495, 5112, 0, new String[]{"The toughest challenge yet.", "Can you survive all 69 waves", "to receive the infernal cape?"}),
        PEST_CONTROL(41226, 2495, 5112, 0, new String[]{"Take on this challenge with", "a few other players to protect", "the void outpost."}),
        CASTLE_WARS(41228, 2440, 3089, 0, new String[]{"Massive wars where you'll have", "to retrieve the enemy flag", "to score points."}),
        FISHING_GUILD(41318, 2594, 3417, 0, new String[]{"Here you'll find most of", "the fishing spots in one area,", "next to a nearby bank."}),
        WOODCUTTING_GUILD(41320, 1646, 3505, 0, new String[]{"An area with most of the", "trees in Run-Escape near a bank.", "Home to the redwood tree."}),
        COOKING_GUILD(41322, 3142, 3449, 0, new String[]{"Come here to have access", "to a range next to a bank.", "Great for cooking."}),
        WINTERTODT(41324, 1630, 3948, 0, new String[]{"Fight together to defeat", "the wintertodt for great firemaking", "experience and great loot."}),
        MOTHERLODE_MINE(41326, 3754, 5662, 0, new String[]{"The motherlode mine is a", "great place to afk train some", "mining xp for some nice rewards."}),
        MINING_GUILD(41328, 3017, 9720, 0, new String[]{"You'll find high level ores", "here close to a bank chest.", "Also now has amethyst minerals."}),
        BLAST_FURNACE(41330, 1940, 4959, 0, new String[]{"A great place to buy ores", "and smith them into bars for", "experience and money."}),
        GOD_WARS(41418, 2880, 5310, 0, new String[]{"Home of the four generals,", "take them on to receive valuable", "rare drops."}),
        DKS(41420, 2900, 4449, 0, new String[]{"Take on the 3 dagannoth kings,", "each immune to different attack", "styles. @red@Teleports inside"}),
        KBD(41422, 2271, 4681, 0, new String[]{"The king black dragon, one of", "the toughest dragons on here.", "Known for its pet drop."}),
        KALPHITE_QUEEN(41424, 3508, 9493, 0, new String[]{"The kalphite queen is a", "2 phase enemy, known for its", "dragon chainbody and head drops."}),
        GIANT_MOLE(41426, 1752, 5237, 0, new String[]{"The giant mole is one", "of the easier bosses, dropping", "valuable herblore supplies."}),
        KRAKEN(41428, 3696, 5807, 0, new String[]{"The kraken is a slayer boss", "known for its trident of the", "seas and tentacle drops."}),
        ZULRAH(41430, 2199, 3056, 0, new String[]{"One of the tougher solo", "bosses, drops valuable supplies.", "Great for ironmen."}),
        SKOTIZO(41432, 1693, 9886, 0, new String[]{"Skotizo requires a full totem", "to kill, dropping garunteed", "clue scrolls and more."}),
        CALLISTO(41434, 3281, 3836, 0, new String[]{"Callisto is one of the 4", "wilderness bosses.", "@red@WARNING:@whi@ level 41 wilderness."}),
        CHAOS_ELE(41436, 3283, 3917, 0, new String[]{"Chaos elemental is an easy", "boss located near the rogues. ", "@red@WARNING:@whi@ level 50 wilderness."}),
        SCORPIA(41438, 3236, 10333, 0, new String[]{"Scorpia is one of the 4", "wilderness bosses.", "@red@WARNING:@whi@ level 50 wilderness."}),
        VET_ION(41440, 3215, 3772, 0, new String[]{"Vet'ion is one of the 4", "wilderness bosses.", "@red@WARNING:@whi@ level 32 wilderness."}),
        CERBERUS(41442, 1240, 1244, 0, new String[]{"Cerberus is a high level", "slayer monster, known for", "its boot upgrade drops."}),
        CORP_BEAST(41444, 2969, 4383, 2, new String[]{"Corporeal beast is a group boss", "known for its valuable", "sigil pieces."}),
        ABYSSAL_SIRE(41446, 3038, 4766, 0, new String[]{"Abyssal sire is a high level", "slayer boss, rarely dropping unsired", "to hand in for rewards."}),
        BRIMHAVEN(41718, 2711, 9564, 0, new String[]{"Contains a variety of slayer", "monsters up to steel dragons.", ""}),
        WATERFALL(41720, 2575, 9873, 0, new String[]{"Houses a few monsters", "mainly being fire giants.", ""}),
        TAVERLY(41722, 2884, 9798, 0, new String[]{"Contains a few high level", "enemies such as blue dragons,", "black demons and hellhounds."}),
        ICE_DUNGEON(41724, 3023, 9581, 0, new String[]{"Contains the dangerous", "skeletal wyverns, known for", "their valuable supply drops."}),
        EDGEVILLE_DUNG(41726, 3097, 9868, 0, new String[]{"Home to vannaka, the low", "level slayer master, and a", "few slayer monsters."}),
        WATERBIRTH(41728, 2444, 10145, 0, new String[]{"Home to the dagannoths,", "great for cannoning dagannoths", "while on a task."}),
        FREMENNIK(41730, 2806, 10001, 0, new String[]{"Contains a variety of", "exclusive slayer monsters only", "found in Rellekka."}),
        KOUREND(41732, 1666, 10049, 0, new String[]{"Contains most slayer monsters", "who also drop totem pieces.", "Required for Skotizo."}),
        CAVE_HORRORS(41734, 3747, 9373, 0, new String[]{"Home to the cave horrors,", "known for their black mask", "drop, required for slayer helm."}),
        STRONGHOLD(41736, 2428, 9824, 0, new String[]{"Houses a variety of slayer", "monsters.", ""}),
        CHASM_OF_FIRE(41738, 1439, 10079, 1, new String[]{"Home to a large variety of", "demons all located in this", "chasm."}),
        KALPHITE_HIVES(41740, 3305, 9497, 0, new String[]{"Houses various kalphite", "creatures all in one area.", ""}),
        SMOKE_DEVILS(41744, 3724, 5798, 0, new String[]{"Home to the smoke devils", "and the smoke devil", "slayer boss."}),;

        Loc(int button, int x_coord, int y_coord, int z_coord, String[] description_data) {
            this.button_id = button;
            this.x = x_coord;
            this.y = y_coord;
            this.z = z_coord;
            this.description = description_data;
            this.name = (toString().toLowerCase().replaceAll("__", "-").replaceAll("_", " "));
        }

        private String name;

        private int button_id;

        private int x;

        private int y;

        private int z;

        private String[] description;

        static Map<Integer, Loc> types = new HashMap<Integer, Loc>();

        static {
            for (Loc type : Loc.values()) {
                types.put(type.button_id, type);
            }
        }
    }
}
