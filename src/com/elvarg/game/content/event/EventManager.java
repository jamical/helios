package com.elvarg.game.content.event;

import com.elvarg.game.World;
import com.elvarg.game.content.event.impl.BossSpawn;
import com.elvarg.game.content.event.impl.DefaultEvent;
import com.elvarg.game.content.event.impl.DoubleXP;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.util.Misc;
import com.elvarg.Server;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Global event manager
 *
 * @author trees
 */
public class EventManager {

    public static void init() {
        setNewEvent();
        process();
    }

    public static void setNewEvent() {
        int random_event = Misc.getRandom(13);
        switch (random_event) {
            case 1:
                setEvent(new BossSpawn());
                break;
            case 3:
                setEvent(new DoubleXP());
                break;
            default:
                setEvent(new DefaultEvent());
                break;
        }
        process();
    }

    public static void process() {
        if (running) {
            return;
        }
        event.start();
        running = true;
        SERVICE.submit(new Runnable() {
            public void run() {
                try {
                    while (running) {
                        if (getEvent().isFinished()) {
                            terminate();
                            reset();
                        }
                        if (event == null) {
                            System.out.println("ERROR : Terminating Event Manager : Event is null");
                            terminate();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void reset() {
        setEvent(null);
        setNewEvent();
    }

    public static Event event;

    public static final ExecutorService SERVICE = Executors.newSingleThreadExecutor();

    private static boolean running;

    private static void terminate() {
        running = false;
    }

    public static Event getEvent() {
        return event;
    }

    public static void setEvent(Event e) {
        event = e;
    }

}
