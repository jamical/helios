package com.elvarg.game.content.event;

import com.elvarg.game.model.SecondsTimer;

/**
 * Event
 *
 * @author trees
 */
public interface Event {

    public abstract String getName();

    public final SecondsTimer timer = new SecondsTimer();

    public abstract boolean isFinished();

    public abstract void start();

    public abstract void stop();

}
