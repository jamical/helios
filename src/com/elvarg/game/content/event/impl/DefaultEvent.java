package com.elvarg.game.content.event.impl;

import com.elvarg.Server;
import com.elvarg.game.content.event.Event;

/**
 * Event instance
 *
 * @author trees
 */
public class DefaultEvent implements Event {

    @Override
    public boolean isFinished() {
        return timer.finished();
    }

    @Override
    public void start() {
        Server.getLogger().info("Running default event");
        timer.start(360);
    }

    @Override
    public void stop() {
        timer.stop();
    }

    public String getName() {
        return "None";
    }
}
