package com.elvarg.game.content.event.impl;

import com.elvarg.Server;
import com.elvarg.game.World;
import com.elvarg.game.content.event.Event;
import com.elvarg.game.entity.impl.npc.NPC;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.Position;

public class BossSpawn implements Event {

    Position position = new Position(3100, 3500);
    NPC boss = new NPC(6324, position);

    @Override
    public boolean isFinished() {
        return timer.finished();
    }

    @Override
    public void start() {
        timer.start(660);
        for (Player o : World.getPlayers()) {
            if (o != null) {
                if (boss != null) {
                    boss.onAdd();
                    o.getPacketSender().sendMessage("Boss has spawned at home!");
                }
            }
        }
        Server.getLogger().info("Running boss event : " + boss.getId() + " Name " + boss.getDefinition().getName() + " Combat " + boss.getDefinition().getCombatLevel());
    }

    @Override
    public void stop() {
        timer.stop();
        boss.appendDeath();
        for (Player o : World.getPlayers()) {
            if (o != null) {
                o.getPacketSender().sendMessage("Boss has been killed.");
            }
        }
    }

    public String getName() {
        return "Boss Spawn";
    }
}
