package com.elvarg.game.content.event.impl;

import com.elvarg.game.GameConstants;
import com.elvarg.game.World;
import com.elvarg.game.content.event.Event;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.Server;

/**
 * Event instance
 *
 * @author trees
 */
public class DoubleXP implements Event {

    @Override
    public boolean isFinished() {
        return timer.finished();
    }

    @Override
    public void start() {
        for (Player o : World.getPlayers()) {
            if (o != null) {
                o.getPacketSender().sendMessage("<img=13>Double XP event is enabled, this will for @dre@" + (360 / 60) + "@bla@ minutes!");
            }
        }
        timer.start(240);
        Server.getLogger().info("Running doublexp event");
        GameConstants.setIsDoubleXp(true);
    }

    @Override
    public void stop() {
        timer.stop();
        GameConstants.setIsDoubleXp(false);
    }

    public String getName() {
        return "Double XP";
    }

}
