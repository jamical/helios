package com.elvarg.game.content;

import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.PlayerRights;
import com.elvarg.util.Misc;

public class CollectionHandler {

    public static void takeAll(Player player) {
        for (int j2 = 0; j2 < player.getCollections().getItems().length; j2++) {
            if (player.getInventory().getFreeSlots() > 0) {
                player.getInventory().add(player.getCollections().forSlot(j2).getId(), player.getCollections().forSlot(j2).getAmount());
                player.getCollections().delete(player.getCollections().forSlot(j2).getId(), player.getCollections().forSlot(j2).getAmount());
            } else {
                player.getPacketSender().sendMessage("You didn't have enough room to take all your items.");
                return;
            }
        }
        player.getCollections().refreshItems();
        player.getPacketSender().sendMessage("Your vault now contains " + player.getCollections().getValidItems().size() + " items worth " + player.getCollections().getTotalValue() + ".");
    }

    public static void addCollections(Player player) {
        int set = 0;
        if (player.getCollections().isFull()) {
            return;
        }
        if (player.getRights() == PlayerRights.LEGENDARY_DONATOR) {
            player.collections.start(160);
            set = 4;
        } else if (player.getRights() == PlayerRights.SUPER_DONATOR) {
            player.collections.start(260);
            set = 3;
        } else if (player.getRights() == PlayerRights.DONATOR) {
            player.collections.start(360);
            set = 2;
        } else {
            player.collections.start(560);
            set = player.getRights() == PlayerRights.DONATOR ? 1 : 0;
        }
        switch (set) {
            case 4:
                for (int j2 = 0; j2 < set + 1; j2++) {
                    player.getCollection().add(CollectionItems.ITEM_SET_4.getItems()[Misc.getRandom(CollectionItems.ITEM_SET_4.getItems().length)], Misc.getRandom(14));
                }
                break;
            case 2:
                for (int j2 = 0; j2 < set + 1; j2++) {
                    player.getCollection().add(CollectionItems.ITEM_SET_2.getItems()[Misc.getRandom(CollectionItems.ITEM_SET_2.getItems().length)], Misc.getRandom(14));
                }
                break;
            case 1:
                for (int j2 = 0; j2 < set + 1; j2++) {
                    player.getCollection().add(CollectionItems.ITEM_SET_1.getItems()[Misc.getRandom(CollectionItems.ITEM_SET_1.getItems().length)], Misc.getRandom(14));
                }
                break;
            default:
                for (int j2 = 0; j2 < set + 1; j2++) {
                    player.getCollection().add(CollectionItems.ITEM_SET_0.getItems()[Misc.getRandom(CollectionItems.ITEM_SET_0.getItems().length)], Misc.getRandom(14));
                }
                break;
        }
        player.getPacketSender().sendMessage("Your collections vault has been updated.");
    }
}
