package com.elvarg.sql;

import com.elvarg.game.GameConstants;

import java.sql.Connection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MySQLController {

    public static final ExecutorService SQL_SERVICE = Executors.newSingleThreadExecutor();

    public static void toggle() {
        if (GameConstants.MySQL_ENABLED) {
            MySQLProcessor.terminate();
            CONTROLLER = null;
            DATABASES = null;
            GameConstants.MySQL_ENABLED = false;
        } else if (!GameConstants.MySQL_ENABLED) {
            init();
            GameConstants.MySQL_ENABLED = true;
        }
    }

    private static MySQLController CONTROLLER;

    public static void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CONTROLLER = new MySQLController();
    }

    public static MySQLController getController() {
        return CONTROLLER;
    }


    public enum Database {
        RSC
    }

    private static MySQLDatabase[] DATABASES = new MySQLDatabase[2];

    public MySQLDatabase getDatabase(Database database) {
        return DATABASES[database.ordinal()];
    }


    public MySQLController() {
        long startup = System.currentTimeMillis();
        DATABASES = new MySQLDatabase[]{
                new MySQLDatabase("run-escape.net", 3306, "runescap_rsc", "runescap_host", "Us0l*5E9hD4D")
        };
        System.out.println("Connected to SQL Database " + (System.currentTimeMillis() - startup) + "ms");

        MySQLProcessor.process();
    }

    private static class MySQLProcessor {

        private static boolean running;

        private static void terminate() {
            running = false;
        }

        public static void process() {
            if (running) {
                return;
            }
            running = true;
            SQL_SERVICE.submit(new Runnable() {
                public void run() {
                    try {
                        while (running) {
                            if (!GameConstants.MySQL_ENABLED) {
                                terminate();
                                return;
                            }
                            for (MySQLDatabase database : DATABASES) {

                                if (!database.active) {
                                    continue;
                                }

                                if (database.connectionAttempts >= 5) {
                                    database.active = false;
                                }

                                Connection connection = database.getConnection();
                                try {
                                    connection.createStatement().execute("/* ping */ SELECT 1");
                                } catch (Exception e) {
                                    database.createConnection();
                                }
                            }
                            Thread.sleep(25000);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
