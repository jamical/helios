package com.elvarg.sql;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLDatabase {
    public boolean active;
    public int connectionAttempts;
    public String host;
    public int port;
    public String database;
    public String username;
    public String password;
    private Connection connection;

    public MySQLDatabase(String host, int port, String database, String username, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        this.active = true;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void createConnection() {
        try {
            setConnection(DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.username, this.password));
            this.connectionAttempts = 0;
        } catch (Exception e) {
            System.out.println("Unable to create connection to database " + this.database + "!");
            this.connectionAttempts += 1;
            e.printStackTrace();
        }
    }

    public void restart() {
        this.connectionAttempts = 0;
        this.active = true;
    }
}
