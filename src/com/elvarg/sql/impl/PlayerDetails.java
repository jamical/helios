package com.elvarg.sql.impl;

import com.elvarg.game.GameConstants;
import com.elvarg.game.entity.impl.player.Player;
import com.elvarg.game.model.PlayerRights;
import com.elvarg.sql.MySQLController;
import com.elvarg.sql.MySQLDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PlayerDetails {

    public static boolean playerIsRegistered(Player player) {
        MySQLDatabase rsc = MySQLController.getController().getDatabase(MySQLController.Database.RSC);
        try {
            String selectSQL = "SELECT * FROM users WHERE username = '" + player.getUsername() + "'";
            PreparedStatement preparedStatement = rsc.getConnection().prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery(selectSQL);
            while (rs.next()) {
                String username = rs.getString("username");
                if (username.equalsIgnoreCase(player.getUsername())) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setPlayerDetails(Player player) {
        MySQLDatabase rsc = MySQLController.getController().getDatabase(MySQLController.Database.RSC);
        try {
            String selectSQL = "SELECT * FROM users WHERE username = '" + player.getUsername() + "'";
            PreparedStatement preparedStatement = rsc.getConnection().prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery(selectSQL);
            while (rs.next()) {
                String username = rs.getString("username");
                String password = rs.getString("password");
                String age = rs.getString("age");
                int donation_status = rs.getInt("donator");
                int account_rights = rs.getInt("acc_status");
                String date_registered = rs.getString("reg_date");

                if (username.equalsIgnoreCase(player.getUsername())) {
                    if (username != null) {
                        player.setForum_username(username);
                    }
                    if (password != null) {
                        player.setForums_password(password);
                    }
                    if (age != null) {
                        player.setAge(age);
                    }
                    if (date_registered != null) {
                        player.setDate_registered(date_registered);
                    }
                    //RIGHTS
                    if (GameConstants.isDeveloper(player.getUsername().toLowerCase())) {
                        player.setRights(PlayerRights.DEVELOPER);
                        return;
                    } else if (account_rights == 4) {
                        player.setRights(PlayerRights.ADMINISTRATOR);
                        return;
                    } else if (donation_status == 1) {
                        player.setRights(PlayerRights.DONATOR);
                        return;
                    } else {
                        player.setRights(PlayerRights.PLAYER);
                    }
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void get(Player player) {
        MySQLDatabase rsc = MySQLController.getController().getDatabase(MySQLController.Database.RSC);
        if ((!rsc.active) || (rsc.getConnection() == null)) {
            return;
        }
        try {
            PreparedStatement preparedStatement = rsc.getConnection().prepareStatement("SELECT `password` FROM `users` WHERE `username` = " + player.getUsername());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
