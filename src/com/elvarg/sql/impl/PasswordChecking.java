package com.elvarg.sql.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PasswordChecking {

    public static int checkUser(String user, String pass) {
        try {
            String urlString = "http://www.run-escape.net/servValidate.php?name="
                    + user.replace("_", "%20").toLowerCase() + "&pass=" + pass;
            HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = in.readLine().trim();
            try {
                int returnCode = Integer.parseInt(line);
                switch (returnCode) {
                    case 1:
                        System.out.println("Invalid password");
                        return 3;
                    case 0:
                        System.out.println("member doesnt exist");
                        return 12;
                    default:
                        System.out.println("login good");
                        return 2;
                }
            } catch (Exception e) {
                System.out.println(line);
                return 8;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return 11;
    }
}
