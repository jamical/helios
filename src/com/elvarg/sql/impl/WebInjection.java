package com.elvarg.sql.impl;

import com.elvarg.game.World;
import com.elvarg.game.model.SecondsTimer;
import com.elvarg.sql.MySQLController;
import com.elvarg.sql.MySQLDatabase;

import java.sql.PreparedStatement;

public class WebInjection {

    public static final SecondsTimer timer = new SecondsTimer();
    // create our java preparedstatement using a sql update query

    public static boolean sendWorldDetails() {
        int players = World.getPlayers().size();
        MySQLDatabase rsc = MySQLController.getController().getDatabase(MySQLController.Database.RSC);
        if (rsc.active && rsc.getConnection() != null) {
            try {
                PreparedStatement preparedStmt = rsc.getConnection().prepareStatement("UPDATE serverworld SET players_online = '" + players + "' WHERE world_id = 301");
                // call executeUpdate to execute our sql update statement
                preparedStmt.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
