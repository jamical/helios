package com.elvarg;

import com.elvarg.game.GameConstants;
import com.elvarg.game.definition.loader.impl.FileServer;
import com.elvarg.util.flood.Flooder;
import com.google.common.base.Preconditions;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server
{
	private static Logger logger = Logger.getLogger(Server.class.getSimpleName());
	private static boolean isUpdating;
	private static Flooder flooder = new Flooder();

	private Server()
	{
		throw new UnsupportedOperationException("This class cannot be instantiated!");
	}

	public static void main(String[] args)
	{
		try
		{
			if (GameConstants.JAGGRAB_ENABLED) {
				FileServer.init();
			}
			Preconditions.checkState(args.length == 0, "No runtime arguments needed!");
			logger.info("Initializing the Bootstrap...");
			Bootstrap bootstrap = new Bootstrap(43595);
			bootstrap.bind();
			logger.info("The Bootstrap has been bound, runescape is now online!");
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, "An error occurred while binding the Bootstrap!", e);

			System.exit(1);
		}
	}

	public static Logger getLogger()
	{
		return logger;
	}

	public static boolean isUpdating()
	{
		return isUpdating;
	}

	public static void setUpdating(boolean isUpdating)
	{
		isUpdating = isUpdating;
	}

	public static Flooder getFlooder()
	{
		return flooder;
	}
}
